package grupo3.backendProyectoIntegrador.service.imp;

import grupo3.backendProyectoIntegrador.entity.Image;
import grupo3.backendProyectoIntegrador.entity.Image;
import grupo3.backendProyectoIntegrador.repository.ImageRepository;
import grupo3.backendProyectoIntegrador.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ImageService implements IService<Image> {
    private final ImageRepository imageRepository;

    @Autowired
    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Image save(Image image) {
        return imageRepository.save(image);
    }

    @Override
    public Optional<Image> findById(Long id) {
        return imageRepository.findById(id);
    }

    @Override
    public List<Image> findAll() {
        return imageRepository.findAll();
    }

    @Override
    public Image update(Image image) {
        Image i = imageRepository.findById(image.getId()).get();
        i.setTitle(image.getTitle());
        i.setUrl(image.getUrl());
        i.setProducts(image.getProducts());
        return imageRepository.save(i);
    }

    @Override
    public void delete(Long id) {
        if(imageRepository.findById(id).isPresent())
            imageRepository.deleteById(id);
        else
            System.out.println("Non-existent image");
    }
}
