package grupo3.backendProyectoIntegrador.service.imp;

import grupo3.backendProyectoIntegrador.entity.Category;
import grupo3.backendProyectoIntegrador.repository.CategoryRepository;
import grupo3.backendProyectoIntegrador.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService implements IService<Category> {
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category update(Category category) {
        Category c = categoryRepository.findById(category.getId()).get();
        c.setTitle(category.getTitle());
        c.setDescription(category.getDescription());
        c.setImageURL(category.getImageURL());
        return categoryRepository.save(c);
    }

    @Override
    public void delete(Long id) {
        if(categoryRepository.findById(id).isPresent())
            categoryRepository.deleteById(id);
        else
            System.out.println("Non-existent category");
    }
}
