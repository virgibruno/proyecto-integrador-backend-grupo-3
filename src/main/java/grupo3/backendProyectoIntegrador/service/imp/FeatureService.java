package grupo3.backendProyectoIntegrador.service.imp;

import grupo3.backendProyectoIntegrador.entity.Feature;
import grupo3.backendProyectoIntegrador.repository.FeatureRepository;
import grupo3.backendProyectoIntegrador.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FeatureService implements IService<Feature> {
    private final FeatureRepository featureRepository;

    @Autowired
    public FeatureService(FeatureRepository featureRepository) {
        this.featureRepository = featureRepository;
    }

    @Override
    public Feature save(Feature feature) {
        return featureRepository.save(feature);
    }

    @Override
    public Optional<Feature> findById(Long id) {
        return featureRepository.findById(id);
    }

    @Override
    public List<Feature> findAll() {
        return featureRepository.findAll();
    }

    @Override
    public Feature update(Feature feature) {
        Feature f = featureRepository.findById(feature.getId()).get();
        f.setName(feature.getName());
        f.setIcon(feature.getIcon());
        f.setProducts(feature.getProducts());
        return featureRepository.save(f);
    }

    @Override
    public void delete(Long id) {
        if(featureRepository.findById(id).isPresent())
            featureRepository.deleteById(id);
        else
            System.out.println("Non-existent feature");
    }

    public List<Feature> findByProductId(Long id) {
        return featureRepository.findByProductId(id);
    }
}
