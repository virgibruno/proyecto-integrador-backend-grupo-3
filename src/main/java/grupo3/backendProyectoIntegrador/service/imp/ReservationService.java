package grupo3.backendProyectoIntegrador.service.imp;

import grupo3.backendProyectoIntegrador.entity.Product;
import grupo3.backendProyectoIntegrador.entity.Reservation;
import grupo3.backendProyectoIntegrador.repository.ReservationRepository;
import grupo3.backendProyectoIntegrador.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ReservationService implements IService<Reservation> {
    private final ReservationRepository reservationRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Reservation save(Reservation reservation) {return reservationRepository.save(reservation);}

    public List<Reservation> findByProductId(Long id){return reservationRepository.findByProductId(id);}

    public List<Reservation> findByUserId(Long id){return reservationRepository.findByUserId(id);}

    @Override
    public Optional<Reservation> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }

    @Override
    public Reservation update(Reservation reservation) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    public Set<Long> takenIdByDates(Calendar startDate, Calendar endDate) {
        List<Reservation> reservations = this.findAll();
        Set<Long> takenId = new HashSet<>();
        for(Reservation reservation : reservations) {
            if(startDate.before(reservation.getEndDate()) && endDate.after(reservation.getStartDate())){
                takenId.add(reservation.getProduct().getId());
            }
        }
        return takenId;
    }

}
