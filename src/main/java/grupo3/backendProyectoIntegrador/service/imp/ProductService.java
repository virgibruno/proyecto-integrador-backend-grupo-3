package grupo3.backendProyectoIntegrador.service.imp;

import grupo3.backendProyectoIntegrador.entity.Product;
import grupo3.backendProyectoIntegrador.repository.ProductRepository;
import grupo3.backendProyectoIntegrador.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductService implements IService<Product> {
    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product update(Product product) {
        Product p = productRepository.getById(product.getId());
        p.setName(product.getName());
        p.setDescription(p.getDescription());
        p.setCategory(product.getCategory());
        p.setCity(product.getCity());
        p.setFeatures(product.getFeatures());
        p.setImages(product.getImages());
        return productRepository.save(p);
    }

    @Override
    public void delete(Long id) {
        if(productRepository.findById(id).isPresent())
            productRepository.deleteById(id);
        else
            System.out.println("Non-existent product");
    }

    public List<Product> findByCityId(Long id){
        return productRepository.findByCityId(id);
    }

    public List<Product> findByCategoryId(Long id){
        return productRepository.findByCategoryId(id);
    }

    public List<Product> findByNoMatchingId(Set<Long> ids) {
        return productRepository.findByNoMatchingId(ids);
    }

    public List<Product> findByNoMatchingIdAndCityId(Set<Long> ids, Long cityId) {
        return productRepository.findByNoMatchingIdAndCityId(ids, cityId);
    }
}
