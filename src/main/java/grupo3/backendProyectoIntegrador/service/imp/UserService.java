package grupo3.backendProyectoIntegrador.service.imp;

import grupo3.backendProyectoIntegrador.entity.User;
import grupo3.backendProyectoIntegrador.repository.UserRepository;
import grupo3.backendProyectoIntegrador.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IService<User> {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }


    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User update(User user) {
        return null;
    }

    @Override
    public void delete(Long id) {
        if(userRepository.findById(id).isPresent())
            userRepository.deleteById(id);
        else
            System.out.println("Non-existent user");
    }

    public User findByEmail(String mail) {
        return userRepository.findByEmail(mail).orElse(null);
    }

    public User authenticate(User user) {
        User userDb = userRepository.findByEmail(user.getEmail()).orElse(null);
        if(userDb != null) {
            if (passwordEncoder.matches(user.getPassword(), userDb.getPassword())){
                return user;
            }
        }
        return null;
    }
}
