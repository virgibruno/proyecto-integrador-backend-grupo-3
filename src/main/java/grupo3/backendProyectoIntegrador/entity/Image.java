package grupo3.backendProyectoIntegrador.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="images")
public class Image {

    @Id
    @SequenceGenerator(name = "image_sequence", sequenceName = "image_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "image_sequence")
    @Column(name="id_image")
    private Long id;

    // attributes

    private String title;
    private String url;

    //relations

    @ManyToOne
    @JoinColumn(name = "id_product")
    @JsonIgnore
    private Product products;

    // constructor

    public Image() {
    }

    // getters and setters

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Product getProducts() {
        return products;
    }

    public void setProducts(Product products) {
        this.products = products;
    }
}
