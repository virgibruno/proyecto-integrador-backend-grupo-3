package grupo3.backendProyectoIntegrador.entity;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtResponse {
    // Attributes
    private String token;
    private Long id;
    private String email;
    private String firstname;
    private String lastName;
    private Collection<? extends GrantedAuthority> authorities;


    // Constructor
    public JwtResponse(String accessToken, Long id, String email, String firstname, String lastName, Collection<? extends GrantedAuthority> authorities) {
        this.token = accessToken;
        this.id = id;
        this.email = email;
        this.authorities = authorities;
        this.firstname = firstname;
        this.lastName = lastName;
    }

    // Getters and Setters

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return firstname;
    }

    public void setName(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
}
