package grupo3.backendProyectoIntegrador.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="features")
public class Feature {

    @Id
    @SequenceGenerator(name = "feature_sequence", sequenceName = "feature_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "feature_sequence")
    @Column(name="id_feature")
    private Long id;

    // attributes

    private String name;
    private String icon;

    // relations

    @ManyToMany(mappedBy = "features")
    @JsonIgnore
    private Set<Product> products = new HashSet<>();

    // constructor

    public Feature() {
    }

    // getters and setters

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public void addProduct(Product product){
        this.products.add(product);
    }
}
