package grupo3.backendProyectoIntegrador.entity;

import javax.persistence.*;

@Entity
@Table(name="cities")
public class City {

    @Id
    @SequenceGenerator(name = "city_sequence", sequenceName = "city_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "city_sequence")
    @Column(name="id_city")
    private Long id;

    // attributes

    private String name;
    private String country;

    // constructor

    public City() {
    }


    // getters and setters

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
