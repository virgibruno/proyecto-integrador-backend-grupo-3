package grupo3.backendProyectoIntegrador.entity;

import javax.persistence.*;

@Entity
@Table(name="categories")
public class Category {
    
    @Id
    @SequenceGenerator(name = "category_sequence", sequenceName ="category_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_sequence")
    @Column(name="id_category")
    private Long id;

    // attributes

    private String title;
    private String description;
    private String imageURL;

    // constructor

    public Category() {
    }

    // getters and setters

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}

