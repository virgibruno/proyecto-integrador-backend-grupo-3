package grupo3.backendProyectoIntegrador.repository;

import grupo3.backendProyectoIntegrador.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    @Query("FROM Reservation r INNER JOIN r.product p WITH p.id = ?1")
    List<Reservation> findByProductId(Long productId);

    @Query("FROM Reservation r INNER JOIN r.user u WITH u.id = ?1")
    List<Reservation> findByUserId(Long userId);

}
