package grupo3.backendProyectoIntegrador.repository;

import grupo3.backendProyectoIntegrador.entity.Feature;
import grupo3.backendProyectoIntegrador.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FeatureRepository extends JpaRepository<Feature, Long> {
    @Query("FROM Feature f INNER JOIN f.products p WITH p.id = ?1")
    List<Feature> findByProductId(Long cityId);
}
