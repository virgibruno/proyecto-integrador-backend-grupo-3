package grupo3.backendProyectoIntegrador.repository;

import grupo3.backendProyectoIntegrador.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("FROM Product p INNER JOIN p.city c WITH c.id = ?1")
    List<Product> findByCityId(Long cityId);

    @Query("FROM Product p INNER JOIN p.category c WITH c.id = ?1")
    List<Product> findByCategoryId(Long categoryId);

    @Query("FROM Product p WHERE p.id NOT IN ?1")
    List<Product> findByNoMatchingId(Set<Long> ids);

    @Query("FROM Product p INNER JOIN p.city c WITH c.id = ?2 AND p.id NOT IN ?1")
    List<Product> findByNoMatchingIdAndCityId(Set<Long> ids, Long cityId);
}
