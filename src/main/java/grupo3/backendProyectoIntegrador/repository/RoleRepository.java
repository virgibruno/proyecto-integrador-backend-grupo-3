package grupo3.backendProyectoIntegrador.repository;

import grupo3.backendProyectoIntegrador.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
