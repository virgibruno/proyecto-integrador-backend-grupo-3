package grupo3.backendProyectoIntegrador.repository;

import grupo3.backendProyectoIntegrador.entity.Reservation;
import grupo3.backendProyectoIntegrador.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("FROM User u WHERE u.email = ?1")
    Optional<User> findByEmail(String email);
}
