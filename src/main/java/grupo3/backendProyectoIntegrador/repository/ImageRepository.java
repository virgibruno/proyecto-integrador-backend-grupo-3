package grupo3.backendProyectoIntegrador.repository;

import grupo3.backendProyectoIntegrador.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {
}
