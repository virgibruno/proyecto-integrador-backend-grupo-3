package grupo3.backendProyectoIntegrador.repository;

import grupo3.backendProyectoIntegrador.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {
}
