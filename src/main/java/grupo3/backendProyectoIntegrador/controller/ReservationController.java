package grupo3.backendProyectoIntegrador.controller;

import grupo3.backendProyectoIntegrador.entity.Product;
import grupo3.backendProyectoIntegrador.entity.Reservation;
import grupo3.backendProyectoIntegrador.entity.User;
import grupo3.backendProyectoIntegrador.service.imp.ProductService;
import grupo3.backendProyectoIntegrador.service.imp.ReservationService;
import grupo3.backendProyectoIntegrador.service.imp.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reservation")
public class ReservationController {
    private final ReservationService reservationService;
    private final ProductService productService;
    private final UserService userService;

    public ReservationController(ReservationService reservationService, ProductService productService, UserService userService) {
        this.reservationService = reservationService;
        this.productService = productService;
        this.userService = userService;
    }

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody Reservation reservation) {
        Long idProduct = reservation.getProduct().getId();
        Product product = productService.findById(idProduct).orElse(null);
        reservation.setProduct(product);

        Long idUser = reservation.getUser().getId();
        User user = userService.findById(idUser).orElse(null);
        reservation.setUser(user);

        ResponseEntity response;

        if(reservation.getStartDate().before(reservation.getEndDate())){
            response = new ResponseEntity(reservationService.save(reservation), HttpStatus.CREATED);
        } else {
             response = new ResponseEntity("Start Date can't be after End Date", HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @GetMapping("/findByProductId/{productId}")
    public ResponseEntity findByProductId(@PathVariable Long productId){
        ResponseEntity response = new ResponseEntity(reservationService.findByProductId(productId), HttpStatus.FOUND);
        return response;
    }

    @GetMapping("/findByUserId/{userId}")
    public ResponseEntity findByUserId(@PathVariable Long userId){
        ResponseEntity response = new ResponseEntity(reservationService.findByUserId(userId), HttpStatus.FOUND);
        return response;
    }
}
