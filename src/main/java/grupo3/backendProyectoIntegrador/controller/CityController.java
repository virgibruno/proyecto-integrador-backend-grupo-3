package grupo3.backendProyectoIntegrador.controller;

import grupo3.backendProyectoIntegrador.entity.City;
import grupo3.backendProyectoIntegrador.service.imp.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/city")
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("/find")
    public ResponseEntity findAll(){
        ResponseEntity response = new ResponseEntity(cityService.findAll(), HttpStatus.FOUND);
        return response;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity findById(@PathVariable Long id){
        ResponseEntity response;
        if(cityService.findById(id).isPresent()){
            response = new ResponseEntity(cityService.findById(id), HttpStatus.FOUND);
        } else {
            response = new ResponseEntity("Non-existent id", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody City city){
        ResponseEntity response = new ResponseEntity(cityService.save(city), HttpStatus.CREATED);
        return response;
    }


}
