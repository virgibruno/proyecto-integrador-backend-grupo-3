package grupo3.backendProyectoIntegrador.controller;

import grupo3.backendProyectoIntegrador.entity.Category;
import grupo3.backendProyectoIntegrador.service.imp.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody Category category){
        ResponseEntity response = new ResponseEntity(categoryService.save(category), HttpStatus.CREATED);
        return response;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity findById(@PathVariable Long id){
        ResponseEntity response;
        if(categoryService.findById(id).isPresent()){
            response = new ResponseEntity(categoryService.findById(id), HttpStatus.FOUND);
        } else {
            response = new ResponseEntity("Non-existent id", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @GetMapping("/find")
    public ResponseEntity findAll(){
        ResponseEntity response = new ResponseEntity(categoryService.findAll(), HttpStatus.FOUND);
        return response;
    }

    @PatchMapping("/update")
    public ResponseEntity update(@RequestBody Category category){
        ResponseEntity response;
        Category c = categoryService.findById(category.getId()).orElse(null);
        if(c != null){
            response = new ResponseEntity(categoryService.update(category), HttpStatus.OK);
        } else {
            response = new ResponseEntity("Non-existent id", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        ResponseEntity response;
        if (categoryService.findById(id).isPresent()){
            categoryService.delete(id);
            response = new ResponseEntity("Successfully removed", HttpStatus.OK);
        } else {
            response =  new ResponseEntity("Non-existent id", HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
