package grupo3.backendProyectoIntegrador.controller;

import grupo3.backendProyectoIntegrador.entity.*;
import grupo3.backendProyectoIntegrador.jwt.JwtUtil;
import grupo3.backendProyectoIntegrador.service.imp.*;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;
    private final CategoryService categoryService;
    private final CityService cityService;
    private final FeatureService featureService;
    private final ImageService imageService;
    private final ReservationService reservationService;
    private final UserService userService;
    private final JwtUtil jwtUtil;

    @Autowired
    public ProductController(ProductService productService, CategoryService categoryService, CityService cityService, FeatureService featureService, ImageService imageService, ReservationService reservationService, UserService userService, JwtUtil jwtUtil) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.cityService = cityService;
        this.featureService = featureService;
        this.imageService = imageService;
        this.reservationService = reservationService;
        this.userService = userService;
        this.jwtUtil = jwtUtil;
    }

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody Product product, HttpServletRequest request){
        ResponseEntity response;
        String jwt = request.getHeader("Authorization").substring(7);
        Role role = userService.findByEmail(jwtUtil.getUserNameFromJwtToken(jwt)).getRole();

        if(role.getId() == 2) {
            Long idCategory = product.getCategory().getId();
            Category category = categoryService.findById(idCategory).orElse(null);
            product.setCategory(category);

            Long idCity = product.getCity().getId();
            City city = cityService.findById(idCity).orElse(null);
            product.setCity(city);

            Set<Feature> features = new HashSet();
            for(Feature f : product.getFeatures()) {
                features.add(featureService.findById(f.getId()).orElse(null));
            }
            product.setFeatures(features);

            product = productService.save(product);

            Set<Image> images = new HashSet();

            for(Image i : product.getImages()) {
                i.setTitle(product.getName());
                i.setProducts(product);
                images.add(imageService.save(i));
            }

            product.setImages(images);

            response = new ResponseEntity(product, HttpStatus.CREATED);

        } else {
            response = new ResponseEntity("El usuario debe ser administrador para crear un nuevo producto", HttpStatus.UNAUTHORIZED);
        }

        return response;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity findById(@PathVariable Long id){
        ResponseEntity response;
        if(productService.findById(id).isPresent()){
            response = new ResponseEntity(productService.findById(id), HttpStatus.FOUND);
        } else {
            response = new ResponseEntity("Non-existent id", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @GetMapping("/find")
    public ResponseEntity findAll(){
        ResponseEntity response = new ResponseEntity(productService.findAll(), HttpStatus.FOUND);
        return response;
    }

    @GetMapping("/findByCity/{cityId}")
    public ResponseEntity findByCityId(@PathVariable Long cityId){
        ResponseEntity response = new ResponseEntity(productService.findByCityId(cityId), HttpStatus.FOUND);
        return response;
    }

    @GetMapping("/findByCategory/{categoryId}")
    public ResponseEntity findByCategoryId(@PathVariable Long categoryId){
        ResponseEntity response = new ResponseEntity(productService.findByCategoryId(categoryId), HttpStatus.FOUND);
        return response;
    }

    @GetMapping("/getTakenDates/{id}")
    public ResponseEntity getTakenDates(@PathVariable Long id){
        ResponseEntity response;
        Product product = productService.findById(id).orElse(null);
        if(product != null){
            Set<Reservation> reservations = product.getReservations();
            response = new ResponseEntity(reservations, HttpStatus.FOUND);
        } else {
            response = new ResponseEntity("Non-existent id", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @GetMapping("/findByDates/{startYear}-{startMonth}-{startDay}/{endYear}-{endMonth}-{endDay}")
    public ResponseEntity findByDates(@PathVariable int startYear, @PathVariable int startMonth, @PathVariable int startDay, @PathVariable int endYear, @PathVariable int endMonth, @PathVariable int endDay){
        Calendar startDate = new GregorianCalendar(startYear, startMonth-1, startDay);
        Calendar endDate = new GregorianCalendar(endYear,endMonth-1,endDay);
        ResponseEntity response;
        if(startDate.before(endDate)){
            Set<Long> takenId = reservationService.takenIdByDates(startDate, endDate);
            takenId.add(0L);
            List<Product> availableProducts = productService.findByNoMatchingId(takenId);
            response = new ResponseEntity(availableProducts, HttpStatus.CREATED);
        } else {
            response = new ResponseEntity("StartDate must be before EndDate", HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @GetMapping("/findByCityAndDates/{cityId}/{startYear}-{startMonth}-{startDay}/{endYear}-{endMonth}-{endDay}")
    public ResponseEntity findByDatesAndCity(@PathVariable Long cityId, @PathVariable int startYear, @PathVariable int startMonth, @PathVariable int startDay, @PathVariable int endYear, @PathVariable int endMonth, @PathVariable int endDay){
        Calendar startDate = new GregorianCalendar(startYear, startMonth-1, startDay);
        Calendar endDate = new GregorianCalendar(endYear,endMonth-1,endDay);
        ResponseEntity response;
        if(startDate.before(endDate)){
            Set<Long> takenId = reservationService.takenIdByDates(startDate, endDate);
            takenId.add(0L);
            List<Product> availableProducts = productService.findByNoMatchingIdAndCityId(takenId, cityId);
            response = new ResponseEntity(availableProducts, HttpStatus.CREATED);
        } else {
            response = new ResponseEntity("StartDate must be before EndDate", HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}

