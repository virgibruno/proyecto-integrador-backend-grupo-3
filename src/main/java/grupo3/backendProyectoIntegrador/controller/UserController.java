package grupo3.backendProyectoIntegrador.controller;

import grupo3.backendProyectoIntegrador.entity.AuthenticationRequest;
import grupo3.backendProyectoIntegrador.entity.AuthenticationResponse;
import grupo3.backendProyectoIntegrador.entity.JwtResponse;
import grupo3.backendProyectoIntegrador.entity.User;
import grupo3.backendProyectoIntegrador.jwt.JwtUtil;
import grupo3.backendProyectoIntegrador.jwt.UserPrinciple;
import grupo3.backendProyectoIntegrador.service.imp.RoleService;
import grupo3.backendProyectoIntegrador.service.imp.UserService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.HashSet;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    private final RoleService roleService;
    private final JwtUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService, RoleService roleService, JwtUtil jwtUtil, AuthenticationManager authenticationManager, BCryptPasswordEncoder passwordEncoder){
        this.userService = userService;
        this.roleService = roleService;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/signup")
    public ResponseEntity save(@RequestBody User user) {
        ResponseEntity response;
        if (user.getRole() == null){
            user.setRole(roleService.findById(Long.valueOf(1)).orElse(null));
        }
        if ( userService.findByEmail( user.getEmail() ) == null) {

            userService.save(new User (user.getFirstName(), user.getLastName(), user.getEmail(), passwordEncoder.encode(user.getPassword()), user.getRole()));

            response = this.login(user);

        } else {
            response = new ResponseEntity("Duplicate Email", HttpStatus.CONFLICT);
        }
        return response;
    }


    @GetMapping("/find")
    public ResponseEntity findAll(){
        ResponseEntity response = new ResponseEntity(userService.findAll(), HttpStatus.FOUND);
        return response;
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody User user) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtUtil.generateToken(authentication);
        UserDetails userDetails = (UserPrinciple) authentication.getPrincipal();

        user = userService.findByEmail(user.getEmail());

        return new ResponseEntity(new JwtResponse(jwt, user.getId(), userDetails.getUsername(), user.getFirstName(), user.getLastName(), userDetails.getAuthorities()), HttpStatus.OK );
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        ResponseEntity response;
        if (userService.findById(id).isPresent()){
            userService.delete(id);
            response = new ResponseEntity("Successfully removed", HttpStatus.OK);
        } else {
            response =  new ResponseEntity("Non-existent email", HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
