package grupo3.backendProyectoIntegrador.controller;

import grupo3.backendProyectoIntegrador.entity.*;
import grupo3.backendProyectoIntegrador.jwt.JwtUtil;
import grupo3.backendProyectoIntegrador.service.imp.FeatureService;
import grupo3.backendProyectoIntegrador.service.imp.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/feature")
public class FeatureController {
    private final FeatureService featureService;
    private final UserService userService;
    private final JwtUtil jwtUtil;

    @Autowired
    public FeatureController(FeatureService featureService, UserService userService, JwtUtil jwtUtil){
        this.featureService = featureService;
        this.userService = userService;
        this.jwtUtil = jwtUtil;
    }

    @GetMapping("/find")
    public ResponseEntity findAll(){
        ResponseEntity response = new ResponseEntity(featureService.findAll(), HttpStatus.FOUND);
        return response;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity findById(@PathVariable Long id){
        ResponseEntity response;
        if(featureService.findById(id).isPresent()){
            response = new ResponseEntity(featureService.findById(id), HttpStatus.FOUND);
        } else {
            response = new ResponseEntity("Non-existent id", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @GetMapping("/findByProductId/{productId}")
    public ResponseEntity findByProductId(@PathVariable Long productId){
        return new ResponseEntity(featureService.findByProductId(productId), HttpStatus.FOUND);
    }

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody Feature feature, HttpServletRequest request){
        ResponseEntity response;
        String jwt = request.getHeader("Authorization").substring(7);
        Role role = userService.findByEmail(jwtUtil.getUserNameFromJwtToken(jwt)).getRole();

        if(role.getId() == 2) {

            response = new ResponseEntity(featureService.save(feature), HttpStatus.CREATED);

        } else {
            response = new ResponseEntity("El usuario debe ser administrador para agregar una característica", HttpStatus.UNAUTHORIZED);
        }

        return response;
    }
}
